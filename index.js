const {client} = require('./config/AsanaInitialization.js');

class AsanaConnector {
    constructor(token) {
        this.token = token;
    }
    async searchIssue(taskName) {
        console.log("I'm in search issue func");
        // @todo: insert this data dynamically, remove hardcoded stuff
        const allTasks = await client.tasks.getTasks({
            "workspace": "535368881898506",
            "projects": ["1198021824776547"],
            "assignee": "1198023144582788"
        });
        if (allTasks.data.length !== 0) {
            const taskNames = [];
            for (const task of allTasks.data) {
                taskNames.push(task.name);
            }
            ;
            const tasksWithKeyword = taskNames
                .map(task => task.toLowerCase())
                .filter(task => task.includes(taskName))
            if (tasksWithKeyword) {
                console.log(tasksWithKeyword);
                // @todo: return information about task(s) in order to show it in block kit;
                return tasksWithKeyword;
            } else {
                console.log(`Sorry, no tasks with the ${taskName} found`)
                return [];
            }
        } else {
            console.log("Sorry, no tasks found")
            return [];
        }
    }
}

module.exports = {
    AsanaConnector
}