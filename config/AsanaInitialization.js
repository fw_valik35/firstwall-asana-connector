const asana = require('asana');

// @todo: insert this data dynamically, remove hardcoded stuff
const personalAccessToken = '1/1198023144582788:de9e0a62e629f55d848f85243204d273'

const client = asana.Client.create().useAccessToken(personalAccessToken);



async function fetchUserData() {
    const clientInfo = await client.users.me()
    const userGid = await clientInfo.gid
    const userWorkspaces = clientInfo.workspaces;
    return {userWorkspaces, userGid}
}

const {userWorkspaces, userGid} = fetchUserData();
module.exports = {userWorkspaces, userGid, client};